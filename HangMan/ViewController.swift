//
//  ViewController.swift
//  HangMan
//
//  Created by qq_mietek on 28.09.2016.
//  Copyright © 2016 qq_mietekmiszc_kodzenia. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    struct MyData {
        var category = String();
        var subCategory = [String]();
    }
    
    var newData = [MyData]()
    var categoryArray = [String]()
    var subCategoryArray = [[String]]()
    var toViewController = String()
    var qq = [String:NSArray]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var myUrl = URL(string: "http://localhost:3000/category");
        var request = URLRequest(url:myUrl!)
        var task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil
            {
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                let category = json?["category"] as! NSArray
                let subCategory = json?["subCategory"] as! NSArray
                for index in category{
                    self.categoryArray.append(index as! String)
                }
                var iterator = 0
                for _ in subCategory{
                    let subCategoryElement = subCategory[iterator] as! NSArray
                    iterator += 1
                    var tempRow = [String]()
                    for index in subCategoryElement{
                        tempRow.append(index as! String)
                    }
                    self.subCategoryArray.append(tempRow)
                }
                iterator = 0
                for index in self.categoryArray{
                    self.newData.append(MyData(category: index, subCategory: self.subCategoryArray[iterator]))
                    iterator += 1
                }
                self.qq = json as! [String : NSArray]
            } catch {
            }
        }
        task.resume()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var currentRow = 0;
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        while newData.count == 0 {
            
        }
        if component==0 {
            return newData.count
        } else{
            return newData[pickerView.selectedRow(inComponent: 0)].subCategory.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        while newData.count == 0 {
            
        }
        if(component == 0){
            return newData[row].category
        }
        toViewController = newData[pickerView.selectedRow(inComponent: 0)].subCategory[0]
        return newData[pickerView.selectedRow(inComponent: 0)].subCategory[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if newData[pickerView.selectedRow(inComponent: 0)].subCategory.count <= row {
            toViewController = newData[pickerView.selectedRow(inComponent: 0)].subCategory[0]
        } else {
            toViewController = newData[pickerView.selectedRow(inComponent: 0)].subCategory[pickerView.selectedRow(inComponent: 1)]
        }
//        print(String(describing: qq))
//        print("kurwa" + toViewController)
        if currentRow != pickerView.selectedRow(inComponent: 0) {
            currentRow = pickerView.selectedRow(inComponent: 0)
            pickerView.selectRow(0, inComponent: 1, animated: false)
            pickerView.reloadAllComponents()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let wisielec: wiselec = segue.destination as! wiselec
//        for index in qq[toViewController]!{
//            wisielec.wordsArray.append(index as! String)
            wisielec.zmienna = toViewController
//        }

    }

}

