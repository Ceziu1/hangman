//
//  wiselec.swift
//  HangMan
//
//  Created by qq_mietek on 28.09.2016.
//  Copyright © 2016 qq_mietekmiszc_kodzenia. All rights reserved.
//

import UIKit

class wiselec: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var szubienica: UIButton!
    @IBOutlet weak var klawa: UITextField!
       override func viewDidLoad() {
        super.viewDidLoad()
        
        print(zmienna)
        var request = URLRequest(url: URL(string: "http://localhost:3000/" + zmienna)!)
        URLSession.shared.dataTask(with: request){
            data, response, error in
            
            let responseString = String(data: data!, encoding: .utf8)
//            print(responseString!)
            self.word = responseString!
        }.resume()
        szubienica.setTitle("", for: .normal)
        szubienica.setBackgroundImage(UIImage(named: "img/s0.jpg"), for: .normal)
        klawa.delegate = self
        klawa.becomeFirstResponder()
        for _ in 0..<word.characters.count{
            self.pass.text = self.pass.text!+"_ "
        }
//        losuj()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var zmienna = String()
//    var wordsArray = [String]()
    var word = String()
    var usedCharacters = [String]()
    var currentState = 0
    var youWin = 0
    
    @IBOutlet weak var pass: UILabel!
    //keyboard
    
//    func losuj(){
//        var temp = String()
//        temp = wordsArray[Int(arc4random_uniform(UInt32(wordsArray.count)))]
//        word = temp
//        print(word)
//        for _ in 0..<temp.characters.count{
//            self.pass.text = self.pass.text!+"_ "
//        }
//    }
//    
    func checkWord(_ char: String){
        var occured = false
        var czyZmiena = 0
        for item in usedCharacters {
            if item == char{
                occured = true
            }
        }
        if occured{
            print("kurwa to już było")
            return
            //taka litera już się pojawiła
            //jebnąć alerta
        }
        if !occured{//taka litera się nie pojawiła w użytych
            print("kurwa tego jeszcze nie było")
            usedCharacters.append(char)
        }
        print("kurwa " + String(describing: usedCharacters))
        self.pass.text = ""
        occured = false
        for index in word.characters{
            for item in usedCharacters {
                if item.characters.sorted()[0] == index{
                    occured = true
                }
            }
            if String(index) == usedCharacters.last{
                print("kurwa " + String(index))
                czyZmiena += 1
            }
            if occured{
                self.pass.text = self.pass.text!+String(index)+" "
                youWin += 1
            } else {
                self.pass.text = self.pass.text!+"_ "
            }
            occured = false
        }
        if czyZmiena == 0{
            currentState += 1
            print("img/s" + String(currentState) + ".jpg")
            szubienica.setBackgroundImage(UIImage(named: "img/s" + String(currentState) + ".jpg"), for: .normal)
        }
        gameOver()
        czyZmiena = 0
        youWin = 0
    }
    
    func gameOver(){
        var alert = UIAlertController()
        if currentState == 9 {
            alert = UIAlertController(title: "Game Over",
                                          message: "Przegrałeś. Hasło: " + word,
                                          preferredStyle: .alert)
        } else if youWin == word.characters.count {
            alert = UIAlertController(title: "Winner",
                                          message: "Wygrałeś życie... Ale i tak umrzesz",
                                          preferredStyle: .alert)
        } else{
            return
        }
        let dismissHandler = {
            (action: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
            let vc : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "main"))!
            self.show(vc, sender: vc)
        }
        alert.addAction(UIAlertAction(title: "Wróć",
                                      style: .cancel,
                                      handler: dismissHandler))
        present(alert, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 1
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text == ""{
            return false
        }
        checkWord(textField.text!)
        textField.text = ""
        return true
    }
}
