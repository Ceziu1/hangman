var http = require('http');

var allIn = {
	"category": ["Biologia", "Fizyka", "Polski", "Matematyka"]
	, "subCategory": [
    ["ekologia", "genetyka", "weterynaria"]
    , ["kinematyka", "elektryka", "astronomia", "optyka"]
    , ["antyk", "średniowiecze", "renesans"]
    , ["propabilistyka", "funkcje"]
  ]
	, "ekologia": ["populacja", "biosfera", "ekosystem"]
	, "genetyka": ["genotyp", "allel", "chromosom"]
	, "weterynaria": ["kaput", "collum", "truncus"]
	, "kinematyka": ["prędkość", "przyspieszenie", "opóźnienie"]
	, "elektryka": ["napięcie", "natężenie", "ładunek"]
	, "astronomia": ["galaktyka", "gwiazda", "planeta"]
	, "optyka": ["soczewka", "interferencja", "dyfrakcja"]
	, "antyk": ["nemesis", "katharsis", "topos"]
	, "średniowiecze": ["uniwersalizm", "apokryf", "trubadur"]
	, "renesans": ["humanizm", "stoicyzm", "epikureizm"]
	, "propabilistyka": ["prawdopodobieństwo", "zdarzenie", "omega"]
	, "funckje": ["delta", "dziedzina"]
}

var category = {
	"category": ["Biologia", "Fizyka", "Polski", "Matematyka"]
	, "subCategory": [
    ["ekologia", "genetyka", "weterynaria"]
    , ["kinematyka", "elektryka", "astronomia", "optyka"]
    , ["antyk", "sredniowiecze", "renesans"]
    , ["propabilistyka", "funkcje"]
  ]
}

var pass = {
      "ekologia": ["populacja", "biosfera", "ekosystem"]
	, "genetyka": ["genotyp", "allel", "chromosom"]
	, "weterynaria": ["kaput", "collum", "truncus"]
	, "kinematyka": ["prędkość", "przyspieszenie", "opóźnienie"]
	, "elektryka": ["napięcie", "natężenie", "ładunek"]
	, "astronomia": ["galaktyka", "gwiazda", "planeta"]
	, "optyka": ["soczewka", "interferencja", "dyfrakcja"]
	, "antyk": ["nemesis", "katharsis", "topos"]
	, "sredniowiecze": ["uniwersalizm", "apokryf", "trubadur"]
	, "renesans": ["humanizm", "stoicyzm", "epikureizm"]
	, "propabilistyka": ["prawdopodobieństwo", "zdarzenie", "omega"]
	, "funckje": ["delta", "dziedzina"]
}

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/json'});
  if(req.url == "/")
    res.write(JSON.stringify(allIn));
  else if(req.url == "/category")
    res.write(JSON.stringify(category));
  else
    res.write(pass[req.url.replace("/", "")][Math.floor(Math.random()*pass[req.url.replace("/", "")].length)]);
  res.end();
}).listen(3000);